const path = require('path')
const webpack = require('webpack')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');




module.exports = {
    entry: ["babel-polyfill", "./src/js/index.js"],
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        overlay: true,
        openPage: '/'
    },
    module:{
        rules:[{
            test:/\.js$/,
            loader: 'babel-loader',
            exclude: '/node_modules'
        }, {
            test:/\.scss$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {sourceMap: true}
                }, {
                    loader: 'postcss-loader',
                    options: {sourceMap: true, config: {path: 'postcss.config.js'}}
                }, {
                    loader: 'resolve-url-loader',
                },

                {
                    loader: 'sass-loader',
                    options: {sourceMap: true}
                },
            ]}, {
            test:/\.css$/,
            use: ['style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {sourceMap: true}
                }, {
                    loader: 'postcss-loader',
                    options: {sourceMap: true, config: {path: 'postcss.config.js'}}
                }]
        },{
            test:/\.(png|jpg|gif)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    outputPath: 'images',
                    esModule: false
                }
            },{
                loader: 'image-webpack-loader',
                options: {
                    mozjpeg: {
                        progressive: true,
                        quality: 65
                    },
                    // optipng.enabled: false will disable optipng
                    optipng: {
                        enabled: false
                },
                    pngquant: {
                        quality: [0.65, 0.90],
                        speed: 4
                    },
                    gifsicle: {
                        interlaced: false
                    },
                    // the webp option will enable WEBP
                    webp: {
                        quality: 75
                    }
                }
            }]

        },{
            test: /\.svg$/,
            include: path.resolve(__dirname, 'src/images/svgicons'),
            use: [
                'svg-sprite-loader',
                'svgo-loader'
            ]
        },

            {
                test: /\.(ttf|eot|woff|woff2|svg|otf)$/,
                include: path.resolve(__dirname, './src/fonts'),
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                            esModule: false,
                        }
                    }
                ]
            }
        ]
    },


    plugins:[
        new HTMLWebpackPlugin({
            template: './src/index.html'
        }),

        new CleanWebpackPlugin(),

        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
            // chunkFilename: '[id].css'
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new SpriteLoaderPlugin({}),

        // new CopyWebpackPlugin({
        //     patterns: [
        //         // Fonts:
        //         {
        //             from: 'src/fonts/',
        //             to: 'fonts/'
        //         },
        //     ]
        // }),
    ]
}

