import './main.js'
import 'jquery'
import 'popper.js'
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/collapse';
import '../scss/main.scss'
import '../scss/fonts.scss'

//svg-images
import '../images/svgicons/Polygon.svg';
import '../images/svgicons/home.svg';
import '../images/svgicons/whatsapp.svg';
import '../images/svgicons/Viber.svg';
import '../images/svgicons/instagram.svg';
import '../images/svgicons/VK.svg';
import '../images/svgicons/logo.svg';
import '../images/svgicons/location.svg';
import '../images/svgicons/mail.svg';
import '../images/svgicons/phone-call.svg';
import '../images/svgicons/Polygon_red.svg';
import '../images/svgicons/HamburgerIcon.svg';

